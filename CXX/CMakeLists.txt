
add_library (Yap++ SHARED
    yapa.hh
    yapdb.hh
    yapi.cpp
    yapi.hh
    yapie.hh
    yapq.hh
    yapt.hh
    )

include_directories (H include ${CMAKE_BINARY_DIR} ${GMP_INCLUDE_DIR})

target_link_libraries(Yap++ libYap)

install(TARGETS  Yap++
  LIBRARY DESTINATION ${libdir} )

set( CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES} ${GMP_INCLUDE_DIR} )
#set( CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES} ${GMP_LIBRARIES} )


